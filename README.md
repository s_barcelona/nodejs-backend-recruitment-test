Sngular Barcelona Engineer Recruitment Test
===========================================

Thank you for taking the time to do our technical test. It consists of two parts:

* [A small project coding test](#project-coding-test)
* [A few technical questions](#technical-questions)

We would like you to submit your results in a ZIP file. Please make this a **single** zip file named `{yourname}-backend-recruiting-test.zip` containing:

1. a single markdown file with the answers to the technical questions.
2. a folder containing the project coding test.

## Coding Test

### Task requirements

Feel free to spend as much or as little time on the exercise as you like as long as the following requirements have been met.  

- Please complete the user story below.
- Your code should compile (if necesary) and run in one step.
- NodeJS should be used as a platform.
- Feel free to use whatever frameworks / libraries / packages you like.
- You **must** include tests.

### User Story

As a **user running postman, cURL or a browser**  
I can **call a 'restful' GET endpoint with a parameter containing a spotify album URL**  
So that **I get a JSON containing the title, cover image, songs list, and author**

#### Acceptance criteria

- For the url `https://open.spotify.com/album/0ETFjACtuP2ADo6LFhL6HN`, results are returned.
- The title of the album, cover image URL, author and names of the songs are returned in JSON format.

## Technical questions

Please answer the following questions in a markdown file called `Answers to technical questions.md`.

1. If you had plenty of time and resources, what would you add to the coding test you implemented?
2. How would you track down a performance issue in production? Have you ever had to do this?
3. Please describe yourself and your technical skills using JSON.
4. How do you test your backend? Feel free to comment on your preferred approaches and frameworks.
5. In which of the following use cases Node would be optimal? In which don't?
    - IoT device app that reads data from a sensor and sends it to a central repository.
    - Machine learning object detection.
    - Serverless function fired by an image upload event to generate thumbnails.
    - Parsing and importing a long and complex file format into a relational database.
    - A realtime chat app backend.
    - A single page application (SPA).
    - A bitcoin mining app.


#### Thanks for your time, we look forward to hearing from you!
- The [Sngular Barcelona Tech team](https://sngular.com)